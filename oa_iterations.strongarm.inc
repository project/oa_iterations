<?php

/**
 * Implementation of hook_strongarm().
 */
function oa_iterations_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_oa_iterations_iteration';
  $strongarm->value = 0;

  $export['comment_anonymous_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_oa_iterations_iteration';
  $strongarm->value = '3';

  $export['comment_controls_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_oa_iterations_iteration';
  $strongarm->value = '2';

  $export['comment_default_mode_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_oa_iterations_iteration';
  $strongarm->value = '2';

  $export['comment_default_order_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_oa_iterations_iteration';
  $strongarm->value = '300';

  $export['comment_default_per_page_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_oa_iterations_iteration';
  $strongarm->value = '0';

  $export['comment_form_location_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_oa_iterations_iteration';
  $strongarm->value = '2';

  $export['comment_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_oa_iterations_iteration';
  $strongarm->value = '0';

  $export['comment_preview_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_oa_iterations_iteration';
  $strongarm->value = '1';

  $export['comment_subject_field_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_oa_iterations_iteration';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-1',
    'revision_information' => '3',
    'author' => '4',
    'options' => '5',
    'comment_settings' => '7',
    'menu' => '-4',
    'book' => '2',
    'attachments' => '6',
    'og_nodeapi' => '-3',
  );

  $export['content_extra_weights_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'form_build_id_oa_iterations_iteration';
  $strongarm->value = 'form-e6a0904ed48d39b28e1d5a1a11a4317f';

  $export['form_build_id_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_oa_iterations_iteration';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
    2 => 'revision',
  );

  $export['node_options_oa_iterations_iteration'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_oa_iterations_iteration';
  $strongarm->value = '1';

  $export['upload_oa_iterations_iteration'] = $strongarm;
  return $export;
}
