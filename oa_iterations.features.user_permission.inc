<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function oa_iterations_user_default_permissions() {
  $permissions = array();

  // Exported permission: create oa_iterations_iteration content
  $permissions['create oa_iterations_iteration content'] = array(
    'name' => 'create oa_iterations_iteration content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'manager',
    ),
  );

  // Exported permission: delete any oa_iterations_iteration content
  $permissions['delete any oa_iterations_iteration content'] = array(
    'name' => 'delete any oa_iterations_iteration content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete own oa_iterations_iteration content
  $permissions['delete own oa_iterations_iteration content'] = array(
    'name' => 'delete own oa_iterations_iteration content',
    'roles' => array(),
  );

  // Exported permission: edit any oa_iterations_iteration content
  $permissions['edit any oa_iterations_iteration content'] = array(
    'name' => 'edit any oa_iterations_iteration content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  return $permissions;
}
