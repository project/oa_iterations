<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function oa_iterations_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function oa_iterations_node_info() {
  $items = array(
    'oa_iterations_iteration' => array(
      'name' => t('Iteration'),
      'module' => 'features',
      'description' => t('Create an iteration referencing individual items (nodes). Nodes can be tickets or blog posts or book posts.'),
      'has_title' => '1',
      'title_label' => t('Iteration Name'),
      'has_body' => '1',
      'body_label' => t('Remarks'),
      'min_word_count' => '0',
      'help' => t('Use a consistent naming convention for the Title field of the iteration. In the Remarks area, add any discussion you may have about the iteration.

Add iteration backlog items through the node-reference widgets. Rearrange individual items as necessary.

You optionally can define dates for the Iteration as well.'),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function oa_iterations_views_api() {
  return array(
    'api' => '2',
  );
}
