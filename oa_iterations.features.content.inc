<?php

/**
 * Implementation of hook_content_default_fields().
 */
function oa_iterations_content_default_fields() {
  $fields = array();

  // Exported field: field_oa_iterations_backlog
  $fields['oa_iterations_iteration-field_oa_iterations_backlog'] = array(
    'field_name' => 'field_oa_iterations_backlog',
    'type_name' => 'oa_iterations_iteration',
    'display_settings' => array(
      'weight' => 0,
      'parent' => '',
      '5' => array(
        'format' => 'teaser',
        'exclude' => 0,
      ),
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'blog' => 0,
      'book' => 0,
      'event' => 0,
      'feed' => 0,
      'feed_item' => 0,
      'group' => 0,
      'profile' => 0,
      'casetracker_basic_project' => 0,
      'shoutbox' => 0,
      'casetracker_basic_case' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
      'oa_iterations_iteration' => 0,
    ),
    'advanced_view' => 'oa_iterations_reference',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '80',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_oa_iterations_backlog][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Backlog Items',
      'weight' => 0,
      'description' => 'Type titles of backlog items. They can be Book pages, Blog entries or Tickets. Optionally you can drag referenced items into order of priority.',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_oa_iterations_dates
  $fields['oa_iterations_iteration-field_oa_iterations_dates'] = array(
    'field_name' => 'field_oa_iterations_dates',
    'type_name' => 'oa_iterations_iteration',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => '',
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'date',
    'required' => '0',
    'multiple' => '0',
    'module' => 'date',
    'active' => '1',
    'granularity' => array(
      'year' => 'year',
      'month' => 'month',
      'day' => 'day',
    ),
    'timezone_db' => '',
    'tz_handling' => 'none',
    'todate' => 'required',
    'repeat' => 0,
    'repeat_collapsed' => '',
    'default_format' => 'short',
    'widget' => array(
      'default_value' => 'blank',
      'default_value_code' => '',
      'default_value2' => 'blank',
      'default_value_code2' => '+14 days',
      'input_format' => 'm/d/Y',
      'input_format_custom' => '',
      'increment' => '15',
      'text_parts' => array(),
      'year_range' => '-1:+3',
      'label_position' => 'above',
      'label' => 'Iteration Dates',
      'weight' => '-2',
      'description' => 'Choose dates when the Iteration is happening.',
      'type' => 'date_popup',
      'module' => 'date',
    ),
  );

  // Exported field: field_oa_iterations_discuss
  $fields['oa_iterations_iteration-field_oa_iterations_discuss'] = array(
    'field_name' => 'field_oa_iterations_discuss',
    'type_name' => 'oa_iterations_iteration',
    'display_settings' => array(
      'weight' => '1',
      'parent' => '',
      '5' => array(
        'format' => 'teaser',
        'exclude' => 0,
      ),
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'blog' => 0,
      'book' => 0,
      'event' => 0,
      'feed' => 0,
      'feed_item' => 0,
      'group' => 0,
      'profile' => 0,
      'casetracker_basic_project' => 0,
      'shoutbox' => 0,
      'casetracker_basic_case' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
      'oa_iterations_iteration' => 0,
    ),
    'advanced_view' => 'oa_iterations_reference',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '80',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_oa_iterations_discuss][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Related Discussion',
      'weight' => '1',
      'description' => 'Reference discussion posts related to this Iteration. These will be displayed separately from the Iteration backlog items.',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Backlog Items');
  t('Iteration Dates');
  t('Related Discussion');

  return $fields;
}
